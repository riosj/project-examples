/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.managed;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ValueChangeEvent;
import pe.edu.cibertec.bean.Cliente;

/**
 *
 * @author Jesús Rios
 */
@ManagedBean
public class ClienteBean implements Serializable{
    
    private Cliente cliente = new Cliente();
    
    private String mensaje;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    public String registrar() {
        return "resultado";
    }
    
    public void mensajeProfesion(ValueChangeEvent e) {
        String valor = (String) e.getNewValue();
        if ("001".equals(valor)) {
            setMensaje("Tenemos los mejores cursos de Arquitectura");
        }
        if ("002".equals(valor)) {
            setMensaje("Grandes eventos esperan por ti");
        }
    }
}
