/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.managed;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Jesús Rios
 */
@ManagedBean(name = "reporte")
public class ReporteBean {
    
    private String tipo;
    private String titulo;
    
    public void confirmarAcccion(ActionEvent ae) {
        tipo = (String) ae.getComponent().getAttributes().get("tipo");
        titulo = (String) ae.getComponent().getAttributes().get("titulo");
    }
    
    public String consultar(){
        //Obtener clientes segun tipo
        //cliente=servicio.getClientes(tipo);
        return "reporte";
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    
}
